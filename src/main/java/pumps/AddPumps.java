package pumps;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class AddPumps
{
    WebDriver driver;
	  
    public WebDriver Add_Pumps(WebDriver driver) throws InterruptedException
    
	{
    	
    	 System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    	 
       driver.findElement(By.xpath("//body/app-root[1]/app-dashboard[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/a[1]/div[1]")).click();
		
       Thread.sleep(2000);
       
      
       
       driver.findElement(By.xpath("//button[contains(text(),'Add Pumps')]")).click();
       
     WebElement adduser= driver.findElement(By.xpath("//input[@name='Customer_Name']"));
     Actions User1 = new Actions(driver);
     adduser.sendKeys(Keys.SPACE); 
     Thread.sleep(4000);
     adduser.click();
     Thread.sleep(2000);
     
     for(int i=1;i<=12;i++)
     {
  	   adduser.sendKeys(Keys.ARROW_DOWN); 
    	System.out.println("select value" +i);
     }
     
      adduser.sendKeys(Keys.ENTER);
     Thread.sleep(2000);
     
     System.out.println("Customer name select");
    
       
        driver.findElement(By.xpath("//input[@name='pumpTagNo']")).sendKeys("Test-2022");
       
       Thread.sleep(2000);
       
       System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
       
       driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]")).sendKeys("Sample");
       
       driver.findElement(By.xpath("//input[@name='pumpModel']")).sendKeys("Test-2022");
       
       Thread.sleep(2000);
       
       driver.findElement(By.id("pumpType")).sendKeys("API");
       
       WebElement pumptype=driver.findElement(By.id("Fk_pumpSubtype_id"));
       pumptype.click();
       Thread.sleep(5000);
       pumptype.sendKeys("BB1");
       
       Thread.sleep(2000);
       
       
       driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[7]/div[2]/input[1]")).sendKeys("500");
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//input[@name='customerSerialNo']")).sendKeys("1111111");
       
       driver.findElement(By.xpath("//input[@name='MDPLSerialNo']")).sendKeys("1010101CC");
       
       WebElement unit=driver.findElement(By.xpath("//select[@id='pump_unit']"));
		Actions test = new Actions(driver);
             unit.click();
		 
              unit.sendKeys(Keys.ARROW_DOWN);
	         Thread.sleep(2000);
	         
	         unit.sendKeys(Keys.ARROW_DOWN);
	         Thread.sleep(2000);
	         
	         unit.sendKeys(Keys.ENTER);
	         Thread.sleep(2000);
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[12]/div[2]/select[1]")).sendKeys("No");
       
       driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[13]/div[2]/select[1]")).sendKeys("No");
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//input[@name='PDP_flow']")).sendKeys("120 m3/hr");
       
       driver.findElement(By.xpath("//input[@name='differentialPressure']")).sendKeys("120.00Kg/cm2g");
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//input[@name='PDP_head']")).sendKeys("120m");
       
       driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[17]/div[2]/input[1]")).sendKeys("120 (C)");
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//input[@name='PDP_speed']")).sendKeys("120 rpm");
       
       driver.findElement(By.xpath("//input[@name='PDP_specificGravity']")).sendKeys("1.00");
       
       driver.findElement(By.xpath("//select[@id='abrassiveSolids']")).sendKeys("No");
       
       Thread.sleep(2000);
       
       driver.findElement(By.xpath("//input[@name='PDP_efficiency']")).sendKeys("120(%)");
       
       driver.findElement(By.xpath("//input[@name='PDP_ratedPower']")).sendKeys("10.00 KW");
       
       driver.findElement(By.xpath("//input[@name='PDP_minFlow']")).sendKeys("120 m3/hr");
       
       //-----------------------------------click on save button---------------------------//
       
      // Thread.sleep(4000);
       
      //driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[24]/div[1]/button[2]")).click();
       
       Thread.sleep(3000);
       
     //-----------------------------------back arrow---------------------------//
       
      driver.findElement(By.xpath("//body/app-root[1]/app-add-pumps[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/a[1]")).click();
       
		
		return driver;
		
		
	}

}
