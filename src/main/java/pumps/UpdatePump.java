package pumps;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class UpdatePump
{

	WebDriver driver;
	public WebDriver UpdatePump(WebDriver driver) throws InterruptedException
	
    {
		//---------------------------------Click on moving arrow-------------------//
		
		driver.findElement(By.xpath("//body/app-root[1]/app-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[2]/mat-paginator[1]/div[1]/div[1]/div[2]/button[4]/span[1]/*[1]")).click();
		
		Thread.sleep(4000);
		
		//-------------------------------Click on pumps test user------------------------//
		driver.findElement(By.xpath("//button[contains(text(),'Test-2022')]")).click();
		Thread.sleep(4000);
		
	     //------------------------------------Click on design parameters---------------------//
		driver.findElement(By.xpath("//div[contains(text(),'Design Parameters')]")).click();
		
		//------------------------click on edit option--------------------//
		driver.findElement(By.xpath("//mat-tab-body/div[1]/app-tab-fixed-parameters[1]/div[1]/div[1]/h4[1]/button[1]")).click();
		Thread.sleep(2000);
		
		
		
		//------------------------edit multiple fields----------------------------//
//		WebElement CustomerName=driver .findElement(By.xpath("//input[@id='_Customer_Name']"));
//		 Actions User1 = new Actions(driver);
//		 CustomerName.sendKeys(Keys.SPACE); 
//	     Thread.sleep(4000);
//	     CustomerName.click();
//	     Thread.sleep(2000);
//	     
//	     for(int i=1;i<=12;i++)
//	     {
//	    	 CustomerName.sendKeys(Keys.ARROW_DOWN); 
//	    	System.out.println("select value" +i);
//	     }
//	     
//	     CustomerName.sendKeys(Keys.ENTER);
//     Thread.sleep(2000);
		
		
		WebElement pumptabno=driver.findElement(By.xpath("//input[@name='pumpTagNo']"));
		pumptabno.clear();
		pumptabno.sendKeys("Test-2024");
		
		WebElement pumpmake=driver.findElement(By.xpath("//input[@name='pumpMake']"));
		pumpmake.clear();
		pumpmake.sendKeys("Sample test");
		Thread.sleep(4000);
		
		WebElement pumpmodel=driver.findElement(By.xpath("//input[@name='pumpModel']"));
		pumpmodel.clear();
		pumpmodel.sendKeys("Test data-2022");
		
	    WebElement e=driver.findElement(By.xpath("//body/div[2]/div[2]/div[1]/mat-dialog-container[1]/dialog-edit-fixed-parameters[1]/div[2]/form[1]/div[1]/div[5]/div[2]/select[1]"));
		e.click();
		//e.clear();
         e.sendKeys("ISO");
         
         Thread.sleep(2000);
         
         WebElement pumptype=driver.findElement(By.id("Fk_pumpSubtype_id"));
         pumptype.click();
         Thread.sleep(4000);
         pumptype.sendKeys("BB5");
         
         WebElement c=driver.findElement(By.xpath("//input[@id='apicode']"));
         c.sendKeys("100");
         
         Thread.sleep(2000);
         
         WebElement customersrnumber=driver.findElement(By.xpath("//input[@name='customerSerialNo']"));
         customersrnumber.clear();
         customersrnumber.sendKeys("222222222");
         
         WebElement MDPLserialNo=driver.findElement(By.xpath("//input[@name='MDPLSerialNo']"));
         MDPLserialNo.clear();
         MDPLserialNo.sendKeys("20202020MM");
         
         Thread.sleep(2000);
         
         WebElement Unit=driver.findElement(By.xpath("//select[@id='Fk_unit_Array_id']"));
         Actions test = new Actions(driver);
         Unit.click();
	 
         Unit.sendKeys(Keys.ARROW_DOWN);
         Thread.sleep(2000);
         
         Unit.sendKeys(Keys.ENTER);
         Thread.sleep(2000);
   
          Thread.sleep(2000);
         
         WebElement PumpDashseet=driver.findElement(By.xpath("/html/body/app-root/app-add-pumps/div/div[2]/div[3]/div/form/div/div[12]/div[2]/select"));
        // PumpDashseet.clear();
         PumpDashseet.sendKeys("NO");
         
         WebElement OEMCurve=driver.findElement(By.xpath("//select[@name='OEMCurved']"));
       //  OEMCurve.clear();
         OEMCurve.sendKeys("NO");
         
         Thread.sleep(2000);
          
         WebElement PDPflow=driver.findElement(By.xpath("PDP_flow"));
         PDPflow.clear();
         PDPflow.sendKeys("1000");
         
         WebElement differentialPressure=driver.findElement(By.xpath("//input[@name='differentialPressure']"));
         differentialPressure.clear();
         differentialPressure.sendKeys("100.000 Kg/cm2g");
         
         Thread.sleep(2000);
         
         WebElement PDPhead=driver.findElement(By.xpath("//input[@name='PDP_head']"));
         PDPhead.clear();
         PDPhead.sendKeys("100 m");
         
         WebElement PDPtemperature=driver.findElement(By.xpath("//input[@name='PDP_temperature']"));
         PDPtemperature.clear();
         PDPtemperature.sendKeys("100 C");
         
         Thread.sleep(2000);
         
         WebElement PDPspeed=driver.findElement(By.xpath("//input[@name='PDP_speed']"));
         PDPspeed.clear();
         PDPspeed.sendKeys("100 rpm");
         
         WebElement PDPspecificGravity=driver.findElement(By.xpath("//input[@name='PDP_specificGravity']"));
         PDPspecificGravity.clear();
         PDPspecificGravity.sendKeys("1.00");
         
         Thread.sleep(2000);
         
         WebElement abrassiveSolids=driver.findElement(By.xpath("//select[@name='abrassiveSolids']"));
        // abrassiveSolids.clear();
         abrassiveSolids.sendKeys("No");
         
         WebElement PDPefficiency=driver.findElement(By.xpath("//input[@name='PDP_efficiency']"));
         PDPefficiency.clear();
         PDPefficiency.sendKeys("100");
         
         Thread.sleep(2000);
         
         WebElement PDPratedPower=driver.findElement(By.xpath("//input[@name='PDP_ratedPower']"));
         PDPratedPower.clear();
         PDPratedPower.sendKeys("10");
         
         WebElement PDPminFlow=driver.findElement(By.xpath("//input[@name='PDP_minFlow']"));
         PDPminFlow.clear();
         PDPminFlow.sendKeys("1000");
         
         Thread.sleep(2000);
         
         driver.findElement(By.xpath("//span[contains(text(),'×')]")).click();
		
		return null;
		
	}
}
