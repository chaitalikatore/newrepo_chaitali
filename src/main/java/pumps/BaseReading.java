package pumps;

import javax.xml.xpath.XPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BaseReading
{
	
	WebDriver driver;
	public WebDriver BaseReading(WebDriver driver) throws InterruptedException
	{
		
		driver.findElement(By.xpath("//body/app-root[1]/app-dashboard[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/a[1]/div[1]")).click();
		
	       Thread.sleep(2000);
	       
		
		//---------------------------------Click on moving arrow-------------------//
		
		driver.findElement(By.xpath("//body/app-root[1]/app-pumps[1]/div[1]/div[1]/div[1]/div[3]/div[2]/mat-paginator[1]/div[1]/div[1]/div[2]/button[4]/span[1]/*[1]")).click();
				
		Thread.sleep(4000);
		
		//-------------------------------Click on pumps test user------------------------//
	    driver.findElement(By.xpath("//button[contains(text(),'Test-2022')]")).click();
				
		Thread.sleep(4000);
				
		driver.findElement(By.id("mat-tab-label-3-1")).click();
		
		driver.findElement(By.xpath("//div[@id='mat-tab-label-2-2']")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//button[contains(text(),'Add Base Reading')]")).click();
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys("2 m3/hr");
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[2]/div[2]/input[1]")).sendKeys("20 kg/cm2g");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]")).sendKeys("6");
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[4]/div[2]/input[1]")).sendKeys("80 C");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[5]/div[2]/input[1]")).sendKeys("130 rpm");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-base-readings[1]/div[1]/div[2]/div[3]/div[1]/form[1]/div[1]/div[6]/div[2]/input[1]")).sendKeys("80");
		
		Thread.sleep(3000);
		
		//driver.findElement(By.xpath(""))
		
		
		return driver;
		
	}


}
