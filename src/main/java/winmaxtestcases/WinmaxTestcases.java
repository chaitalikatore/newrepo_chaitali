package winmaxtestcases;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import customers.AddCustomer;
import customers.UpdateCustomer;
import pumps.AddPumps;
import pumps.BaseReading;
import pumps.UpdatePump;
import customers.ChangePassword;
import user.CreateUser;
import user.UpdateUser;
import user.UserChangePassword;
import winmaxlogin.*;

import org.openqa.selenium.WebDriver;

class WinmaxTestcases 
{
	
	 WebDriver driver;
	//WebDriver c;
	
   //----------------------------Login script---------------------------------//
	
	@Test(priority = 1,description = "login")
	public void validate_login_page() throws InterruptedException
	{
	    System.out.println("Hello AddCustomer class - validate_login_page()");
	    LoginWinmax login1 = new LoginWinmax();
	    driver= login1.winmax_login();
	    
	}
	
	//---------------------------------------Add customer script---------------------------//
	
	@Test(priority = 2,description = "Customer")
	
	public void Customer_form() throws InterruptedException
	{
		System.out.println("Hello AddCustomer class - Validate_Add_cutomer()");
		
		AddCustomer customer=new AddCustomer();
		
		customer.AddCustomer(driver);
		//return d;
		//return null;
	}
	
	//-----------------------------------------Update customer script----------------------------//
	
    @Test(priority = 3,description = " Update Customer")
	
	   public void Update_Customer_form() throws InterruptedException
	    {
		  System.out.println("Hello  update Customer class - Update_Add_cutomer()");
		
		  UpdateCustomer update = new UpdateCustomer();
		
		  update.Update_cutomer(driver);
	   }
    
    //------------------------------change password---------------------------------------------//
    
    @Test(priority = 4,description = "customer_change_password")
	
	   public void customer_change_password() throws InterruptedException
	    {
		  System.out.println("Hello change_password_Customer class - customer_change_password()");
		
		  ChangePassword  change = new ChangePassword ();
		
		  change.customer_change_password(driver);
		
	    }
	
    //----------------------------Add user----------------------------//
    @Test(priority = 5,description = "Add_user")
	
	   public void AddUser() throws InterruptedException
	    {
		  System.out.println("Hello Add user class - AddUser()");
		
		  CreateUser  add = new CreateUser();
		
		  add.AddUser(driver);
		
    
	    }
    //--------------------update user-----------------------------------//
    
   @Test(priority = 6,description = "UpdateUser")
	
	   public void UpdateUser() throws InterruptedException
	    {
		  System.out.println("Hello update user class - UpdateUser()");
		
		  UpdateUser update = new UpdateUser();
		
		  update.UpdateUser(driver);
		
	    }
    
    //----------------------------user change password-------------------------//
    
    @Test(priority = 7,description = "user change password")
	
	   public void User_change_password() throws InterruptedException
	    {
		  System.out.println("Hello update user class - User_change_password()");
		
		  UserChangePassword  changepassuser = new UserChangePassword();
		
		  changepassuser.User_change_password(driver);
		  
	    }
    
    //--------------------------Add pump--------------------------//
    @Test(priority = 8,description = "Add pumps")
	
	   public void add_pumps() throws InterruptedException
	    {
		  System.out.println("Hello update add user class - add_pumps()");
		
		  AddPumps pumpadd= new AddPumps();
		
		  pumpadd.Add_Pumps(driver);
		  
	    }
    
  //--------------------------Update pump--------------------------//
    @Test(priority = 9,description = "Update pump")
	
	   public void UpdatePump() throws InterruptedException
	    {
		  System.out.println("Hello Update Pump class - UpdatePump()");
		
		  UpdatePump pump= new UpdatePump();
		
		  pump.UpdatePump(driver);
		  
	    }
    
    //----------------------Pump ->Base reading tab--------------//
    
    @Test(priority = 10,description = "BaseReading")
	
	   public void BaseReading() throws InterruptedException
	    {
		  System.out.println("Hello Update Pump class -BaseReading()");
		
		  BaseReading basedata= new BaseReading();
		
		  basedata. BaseReading(driver);
		  
	    }
    
    }
