package customers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import winmaxlogin.LoginWinmax;

public class UpdateCustomer extends AddCustomer
{
	//WebDriver driver;

	boolean attribute;
	public WebDriver Update_cutomer(WebDriver driver) throws InterruptedException
	{
		
        //Click on page scroll arrow
		driver.findElement(By.xpath("//body/app-root[1]/app-customer[1]/div[1]/div[1]/div[1]/div[3]/div[2]/mat-paginator[1]/div[1]/div[1]/div[2]/button[4]/span[1]/*[1]")).click();
		
		//Click on test record 
		driver.findElement(By.xpath("//button[contains(text(),'Test user by chaitali k')]")).click();
		System.out.println("Customer click working fine");
		Thread.sleep(4000);
		
		//Click on record edit icon
		driver.findElement(By.xpath("//body/app-root[1]/app-customer-details[1]/div[1]/div[1]/div[1]/div[3]/div[1]/p[1]/span[1]")).click();
		Thread.sleep(4000);
		
		//clear and edit all fields
		WebElement customername=driver.findElement(By.xpath("//input[@id='_CustomerName']"));
		customername.clear();
		customername.sendKeys("Test customer by chaitali");
		Thread.sleep(4000);
		
		WebElement customermobile= driver.findElement(By.id("_Customer_mobile_No"));
		//boolean attribute  = customermobile.getAttribute("readonly").equals("true");
		
		if (attribute=customermobile.getAttribute("readonly").equals("true")) 
		{
		
			System.out.println("+++++++++++++Mobile field is uneditable on edit customerform++++++++++++++++++++++++++++++++++");
			
			
		}
        Thread.sleep(4000);
		
		
		WebElement MDcustomercode=driver.findElement(By.xpath("//input[@id='_MDPLCustomerCode']"));
		MDcustomercode.clear();
		MDcustomercode.sendKeys("PP5463");
		
		
		WebElement AddressLine=driver.findElement(By.id("_AddressLine"));
		AddressLine.clear();
		AddressLine.sendKeys("2304, Neelkanth");
		
		
		WebElement StreetProvince=driver.findElement(By.id("_StreetProvince"));
		StreetProvince.clear();
		StreetProvince.sendKeys("XYZ");
		
		
		
		Thread.sleep(4000);
		
		
		WebElement statedropdown=driver .findElement(By.id("_CustomerStateDropdown"));
		statedropdown.click();
		statedropdown.sendKeys("Goa");
		
		Thread.sleep(4000);
		
		
		
		WebElement Plantlocation=driver.findElement(By.id("_plantLocation"));
		Plantlocation.clear();
		Plantlocation.sendKeys("Panjab");
		
		Thread.sleep(4000);
		
		WebElement plantAddress=driver.findElement(By.id("_plantAddress"));
		plantAddress.clear();
		plantAddress.sendKeys("satara");
		
		Thread.sleep(4000);
		
		
		WebElement plantStreet=driver.findElement(By.id("_plantStreet"));
		plantStreet.clear();
		plantStreet.sendKeys("Kolhapur");
		
		Thread.sleep(4000);
		
		WebElement PlantStateDropdown=driver.findElement(By.id("_PlantStateDropdown"));
		PlantStateDropdown.click();
		PlantStateDropdown.sendKeys("Goa");
		
		//Thread.sleep(4000);
		
		WebElement Numberofunit=driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/dialog-edit-customer/div[2]/form/div/div[7]/div[2]/input"));
		Numberofunit.clear();
		Numberofunit.sendKeys("3");
		
		Thread.sleep(2000);
		
		WebElement SegmentDropdown=driver.findElement(By.id("_SegmentDropdown"));
		SegmentDropdown.click();
		//SegmentDropdown.clear();
		SegmentDropdown.sendKeys("Metal");
		
		Thread.sleep(2000);
		
		WebElement BankName=driver.findElement(By.id("_BankName"));
		BankName.clear();
		BankName.sendKeys("Axis");
		
		Thread.sleep(2000);
		
		WebElement BranchName=driver.findElement(By.id("_BranchName"));
		BranchName.clear();
		BranchName.sendKeys("pune");
		
		Thread.sleep(2000);
		
		WebElement IFSCCode=driver.findElement(By.id("_IFSCCode"));
		IFSCCode.clear();
		IFSCCode.sendKeys("SBIN0011513");
		
		Thread.sleep(2000);

		
		WebElement AccountNumber=driver.findElement(By.id("_AccountNumber"));
		AccountNumber.clear();
		AccountNumber.sendKeys("378282246310005");
		
		Thread.sleep(2000);

		
		WebElement GSTNo=driver.findElement(By.id("_GSTNo"));
		GSTNo.clear();
		GSTNo.sendKeys("36AADCB2230M2ZT");
		
		Thread.sleep(2000);

		WebElement totalPumpsToAllocate=driver.findElement(By.id("_totalPumpsToAllocate"));
		totalPumpsToAllocate.clear();
		totalPumpsToAllocate.sendKeys("5");
		
		Thread.sleep(2000);

		
		
		driver.findElement(By.className("close")).click();
		
		//driver.findElement(By.xpath("//body/app-root[1]/app-customer-details[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/a[1]")).click();
		
		return driver;
		
		
		}

}
