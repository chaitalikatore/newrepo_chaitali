package customers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import winmaxlogin.LoginWinmax;

public class AddCustomer 
{
	
	static WebDriver driver;
	
    //public void main(String[] args) throws InterruptedException 
	public WebDriver AddCustomer(WebDriver driver) throws InterruptedException
	    {
		//Click on customer dashboard tab
		//driver.findElement(By.xpath("/html/body/app-root/app-dashboard/div/div[2]/div/div/div[2]/div/div/div[1]/a/div/div[1]")).click();//Click on customer dashboard tab
		driver.findElement(By.xpath("//body/app-root[1]/app-dashboard[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/a[1]/div[1]")).click();
		
		//Click on  add customer button
		driver.findElement(By.xpath("//button[contains(text(),'Add Customer')]")).click();
		Thread.sleep(3000);
		
		//----------------- fill customer page-----------------//
		driver.findElement(By.id("_CustomerName")).sendKeys("Test user by chaitali");
		
		driver.findElement(By.id("_Customer_mobile_No")).sendKeys("9911111111");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_MDPLCustomerCode")).sendKeys("111");
		
		driver.findElement(By.id("_AddressLine")).sendKeys("Pune");
		
		driver.findElement(By.id("_StreetProvince")).sendKeys("parner");
		
		Thread.sleep(2000);
		
		//-----------Customer dropdown handling-----------
		
		driver.findElement(By.id("_CustomerStateDropdown")).sendKeys("maharashtra");
	    // data.click();
		Thread.sleep(2000);
		//Select s=new Select(data);
		//s.selectByVisibleText("maharashtra");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[5]/div[2]/section[1]/div[1]/div[1]/div[1]/input[1]")).sendKeys("123");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[5]/div[2]/section[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys("9922222222");
         driver.findElement(By.id("_plantLocation")).sendKeys("kolkatta");
		
		driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[7]/div[2]/input[1]")).sendKeys("pune");
		
		driver.findElement(By.id("_plantAddress")).sendKeys("supa");
		
		driver.findElement(By.id("_plantStreet")).sendKeys("pune");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_PlantStateDropdown")).sendKeys("maharashtra");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[8]/div[2]/input[1]")).sendKeys("10");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[9]/div[2]/section[1]/div[1]/div[1]/div[1]/input[1]")).sendKeys("Testing");

		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[9]/div[2]/section[1]/div[1]/div[1]/div[3]/input[1]")).sendKeys("9933333333");
		
		driver.findElement(By.xpath("//body/app-root[1]/app-add-customer[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[9]/div[2]/section[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys("data");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_SegmentDropdown")).sendKeys("O & G");
		
		//---------------Bank details------------------//
		
		driver.findElement(By.id("_BankName")).sendKeys("HDFC bank");
		
		driver.findElement(By.id("_BranchName")).sendKeys("Ahemadnagar");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_IFSCCode")).sendKeys("SBIN0011513");
		
		driver.findElement(By.id("_AccountNumber")).sendKeys("374245455400126");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_GSTNo")).sendKeys("09AAACH7409R1Vw");
		
		driver.findElement(By.id("_totalPumpsToAllocate")).sendKeys("10");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_contactPersonName")).sendKeys("Test person");
		
		driver.findElement(By.id("_conatctDesignation")).sendKeys("testing");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_conatctMobile")).sendKeys("9955555555");
		
		driver.findElement(By.id("_conatct_Landline")).sendKeys("0241-5555555");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_conatctEmail")).sendKeys("test@gmail.com");
		
		
	    //----------------------Back arrow and back to dashboard functionality----------------------//
		
		driver.findElement(By.className("icon-back-arrow")).click();
		
		Thread.sleep(5000);
		
		//---------------------click on submit button-----------------------------//
		
		//driver.findElement(By.xpath("/html/body/app-root/app-add-customer/div/div[2]/div[3]/div/form/div/div[14]/div[2]/div/button[3]")).click();
		
		return driver;
		
		
		}
		

	}


