package user;

import java.awt.Desktop.Action;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class CreateUser 
{
	
	static WebDriver driver;
	
	
	public WebDriver AddUser(WebDriver driver) throws InterruptedException 
	{
		
		//---------Click on user main module---------//
		driver.findElement(By.xpath("//body/app-root[1]/app-dashboard[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/a[1]/div[1]")).click();
		
		//---------------Click on add user button---------//
		driver.findElement(By.xpath("//button[contains(text(),'Add User')]")).click();
		
		Thread.sleep(2000);
		
	     WebElement e=driver.findElement(By.id("_Customer_Name"));
         
         Actions User1 = new Actions(driver);
         e.click();
        
         
         for(int i=0;i<=11;i++)
         {
        	 e.sendKeys(Keys.ARROW_DOWN); 
        	// System.out.println("select value" +i);
         }
         
          e.sendKeys(Keys.ENTER);
         Thread.sleep(2000);
         
		
		
		driver.findElement(By.xpath("//input[@id='_name']")).sendKeys("Test demo User");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_Designation")).sendKeys("Manager");
		
	    Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"_Security\"]")).sendKeys("TestUnit1");
		//Unit.click();
		//Unit.sendKeys("TestUnit1");
		
		driver.findElement(By.id("_Mobile_No")).sendKeys("9999999922");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("_Landline_No")).sendKeys("0241-678963");
		
		driver.findElement(By.id("_Email_Address")).sendKeys("Testdemouser@gmail.com");
		Thread.sleep(2000);
		
		WebElement Security=driver.findElement(By.xpath("//body/app-root[1]/app-add-users[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[9]/div[2]/select[1]"));
		// Actions test = new Actions(driver);
		 Security.click();
		//Select s=new Select(Security);
		//s.selectByVisibleText("Write");
		 Security.sendKeys(Keys.ARROW_DOWN);
	         Thread.sleep(2000);
	         
	     Security.sendKeys(Keys.ARROW_DOWN);
	         Thread.sleep(2000);
	         
	     Security.sendKeys(Keys.ENTER);
	         Thread.sleep(2000);
	         
	     //---  --------------------------- click on backarrow-------------------------------//
	         
	         driver.findElement(By.xpath("//body/app-root[1]/app-add-users[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/a[1]")).click();
	         
	         
	      //-----------------------------------click on submit button---------------------//
	         
	        // driver.findElement(By.xpath("//body/app-root[1]/app-add-users[1]/div[1]/div[1]/div[1]/div[3]/div[1]/form[1]/div[1]/div[10]/div[2]/button[1]/span[1]")).click();
	         
	         return driver;
		
	}

}
