package winmaxlogin;


	import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;

	public class LoginWinmax 
	{
		public static WebDriver d;
		public WebDriver winmax_login() throws InterruptedException {
            System.out.println("Hello WinmaxLogin class - winmax_login ()");
            System.setProperty("webdriver.chrome.driver","C:/Users/hp/Downloads/chromedriver_win32/chromedriver.exe");
            WebDriver remoteDriver = new ChromeDriver();
            remoteDriver.get("http://stg.winmax365.com/");
            remoteDriver.manage().window().maximize();
            remoteDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            remoteDriver.findElement(By.id("userName")).sendKeys("7666385004");
            remoteDriver.findElement(By.id("password")).sendKeys("Pass@123");
            remoteDriver.findElement(By.xpath("//button[contains(text(),'SIGN IN')]")).click();
            remoteDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            remoteDriver.findElement(By.linkText("Dashboard")).click();
//            String expectedTitle = "MascotDynamicsFail";
//            String actualTitle = remoteDriver.getTitle();
//            Assert.assertEquals(actualTitle,expectedTitle);
            return remoteDriver;
        }

	}



